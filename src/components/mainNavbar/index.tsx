import React, { useState } from "react";
import JTLogo from "./JTlogo.png";
import MenuIcon from '@mui/icons-material/Menu';
import { AppBar, Toolbar, IconButton, Typography, Box, Button, Menu, MenuList, MenuItem } from "@mui/material";
import Login from "../../pages/Login";
import Register from "../../pages/Register";
import './style.css'
import { useNavigate } from "react-router-dom";

const MainNavbar: React.FC = () => {
    const [anchorNav, setAnchorNav] = useState<null | HTMLElement>(null);
    const [isLoginFormOpen, setIsLoginFormOpen] = useState(false); // State to control the visibility of the login form
    const [isRegisterFormOpen, setIsRegisterFormOpen] = useState(false); // State to control the visibility of the register form
    const navigate = useNavigate();

    const openMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorNav(event.currentTarget);
    };

    const closeMenu = () => {
        setAnchorNav(null);
    };

    const openLoginForm = () => {
        setIsLoginFormOpen(true);
        closeMenu();
    };

    const closeLoginForm = () => {
        setIsLoginFormOpen(false);
    };

    const openRegisterForm = () => {
        setIsRegisterFormOpen(true);
    };

    const AboutHandler = () =>{
        navigate('/loginPage');
    }
    const contactUS = () =>{
        navigate('/loginPage');
    }
    const closeRegisterForm = () => {
        window.alert('Are you sure you want to cancel your registration?');
        setIsRegisterFormOpen(false);
    };

    return (
        <>
            <div className="main-container">
                <AppBar position="static"></AppBar>
                <AppBar position="static">
                    <Toolbar>
                        <Typography color="inherit" sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                            <Button><img src={JTLogo} alt="Logo" style={{ width: '200px', height: '50px' }} /></Button>
                        </Typography>
                        <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
                            <Button color="inherit" onClick={openLoginForm} >Home</Button>
                            <Button color="inherit" onClick={openLoginForm} >Post</Button>
                            <Button color="inherit" onClick={openLoginForm} >User</Button>
                            <Button color="inherit" onClick={AboutHandler} >About Us</Button>
                            <Button color="inherit" onClick={contactUS}>Contact Us</Button>
                            <Button color="inherit" onClick={openLoginForm} >Login</Button>
                        </Box>
                        <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
                            <IconButton size="large" edge="start" color="inherit" onClick={openMenu}>
                                <MenuIcon />
                            </IconButton>
                        </Box>
                        <Menu open={Boolean(anchorNav)} onClose={closeMenu} sx={{ display: { xs: 'flex', md: 'none' } }}>
                            <MenuList>
                                <MenuItem color="inherit" onClick={openLoginForm} >Home</MenuItem>
                                <MenuItem color="inherit" onClick={openLoginForm} >Post</MenuItem>
                                <MenuItem color="inherit" onClick={openLoginForm} >User</MenuItem>
                                <MenuItem color="inherit" onClick={AboutHandler}>About Us</MenuItem>
                                <MenuItem color="inherit">Contact Us</MenuItem>
                                <MenuItem color="inherit" onClick={openLoginForm}>Login</MenuItem>
                            </MenuList>
                        </Menu>
                        <Typography variant="h6" color="inherit" component="div" sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                            <Button><img src={JTLogo} alt="Logo" style={{ width: '200px', height: '50px' }} /></Button>
                        </Typography>
                    </Toolbar>
                </AppBar>
                {isLoginFormOpen && <Login closeDialog={closeLoginForm} openRegisterForm={openRegisterForm} />}
                {isRegisterFormOpen && <Register closeDialog={closeRegisterForm} />}
            </div>
        </>
    );
};

export default MainNavbar;