import React, { useState } from "react";
import JTLogo from "./JTlogo.png";  // Import the image
import MenuIcon from '@mui/icons-material/Menu'; // Import MenuIcon
import { useNavigate } from "react-router-dom";
// import './style.css'



// MUI imports
import { AppBar, Toolbar, IconButton, Typography, Box, Button, Menu, MenuList, MenuItem } from "@mui/material";

const Navbar: React.FC = () => {
    
    const [anchorNav, setAcnhorNav] = useState<null | HTMLElement>(null);
    const navigate = useNavigate();

    // Close Function in Restore down state
    const openMenu = (event:React.MouseEvent<HTMLButtonElement>) => {
        setAcnhorNav(event.currentTarget);
    }
    // Close Function in Restore down state
    const closeMenu = () =>{
        setAcnhorNav(null);
    }

    //Home page button function
    const homeBtnHandler = () =>{
        navigate('/');
    }

    //Post page button function
    const postBtnHandler = () =>{
        navigate('/post');
    }

    //User page button function
    const usersBtnHandler = () =>{
        navigate('/users');
    }

    //Logout button function
    const logOutHandler = () =>{
        localStorage.removeItem("Token")
        navigate('/MainNavbar');
    }


   

    return (
            <AppBar position="static">
            <Toolbar>
                <Typography  color="inherit" sx={{ flexGrow:1, display: { xs: 'none', md: 'flex' } }}>
                    <Button><img src={JTLogo} alt="Logo" style={{ width: '200px', height: '50px' }} /></Button>
                </Typography>
                <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
                    <Button color="inherit" onClick={homeBtnHandler} >Home</Button>
                    <Button color="inherit" onClick={postBtnHandler} >Post</Button>
                    <Button color="inherit" onClick={usersBtnHandler} >User</Button>
                    <Button color="inherit"  >About Us</Button>
                    <Button color="inherit"  >Contact Us</Button>
                    <Button color="inherit" onClick={logOutHandler}>Logout</Button>
                </Box>
                {/* Menu Icon */}
                <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
                    <IconButton size="large" edge="start" color="inherit" onClick={openMenu} >
                        <MenuIcon />
                    </IconButton>
                </Box>
                <Menu open = {Boolean(anchorNav)} onClose={closeMenu} sx={{ display: { xs: 'flex', md: 'none' } }}>
                    <MenuList>
                    <MenuItem color="inherit" onClick={homeBtnHandler} >Home</MenuItem>
                    <MenuItem color="inherit" onClick={postBtnHandler} >Post</MenuItem>
                    <MenuItem color="inherit" onClick={usersBtnHandler} >User</MenuItem>
                    <MenuItem color="inherit">About Us</MenuItem>
                    <MenuItem color="inherit">Contact Us</MenuItem>
                    <Button color="inherit" >Logout</Button>
                    </MenuList>
                </Menu>
                <Typography variant="h6" color="inherit" component="div" sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                    <Button><img src={JTLogo} alt="Logo" style={{ width: '200px', height: '50px' }} /></Button>
                </Typography>
            </Toolbar>
            </AppBar>
    );
};

export default Navbar;
