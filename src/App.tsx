// import { useState } from 'react'
// import reactLogo from './assets/react.svg'
// import viteLogo from '/vite.svg'
import './App.css'
import {
  createBrowserRouter,
  RouterProvider,
}from "react-router-dom";

import Home from "./pages/home";
// import Login from "./pages/Login";
// import Register from "./pages/Register";
import Users from './pages/Users';
import Post from './pages/Post';
import MainNavbar from './components/mainNavbar';
import LoginPage from './pages/About'
import { protectedRoute } from './utils/protectedRoute';


// import Navbar from './components/navbar';

const router = createBrowserRouter([
{
  path: "/",
  element: <Home/>,
  loader: protectedRoute
},
// {
//   path: "/login",
//   element: <Login/>,
  
// },
{
  path: "/MainNavbar",
  element: <MainNavbar/>,
  
},
// {
//   path: "/register",
//   element: <Register/>,
//   loader: protectedRoute
// },
{
  path: "/users",
  element: <Users/>,
  loader: protectedRoute
},
{
  path: "/loginPage",
  element: <LoginPage/>,
  // loader: protectedRoute
},
{
  path: "/post",
  element: <Post/>,
  loader: protectedRoute
},
{
  path: "*",
  element: <div> 404 You Found Me</div> 
},
]);

function App() {
  return (
    <> 
      <RouterProvider router={router} />
    </>
  );
}

export default App
