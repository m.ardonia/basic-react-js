import React, { useState } from "react";
import ToDoForm from "./components/ToDoForm";
import TodoDisplay from "./components/TodoDisplay";
import "./style.css";
import { ITodo } from "./type";
import Navbar from "../../components/navbar";

//MUI imports
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import { Box } from "@mui/material";

const Home: React.FC = () => {
  const [temp_input, setTempInput] = useState('');
  const [temp_message, setTempMessage] = useState('');
  const [input_list, setInputList] = useState<{ id: string, input: string, message: string }[]>([]);
  const [editItem, setEditItem] = useState({id:'', input:'', message:''});
  const [showTable, setShowTable] = useState(false);

  //Input Field Handler
  const inputHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
  const { value } = event.target;
  setTempInput(value);
  };

   //Message Field Handler
    const messageHandler = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    const { value } = event.target;
    setTempMessage(value);
  };

  //Add Button Handler
  const btnAddHandler = () =>{
      // push the value to array
      const newInputList = [
        ...input_list,
        {
          id: `${new Date().getTime() / 1000}`,
          input: temp_input,
          message: temp_message,
        },
      ];

      setInputList(newInputList);
      setTempInput("");
      setTempMessage("");
      alert('List successfully Added!');
     
     // console.log(newInputList);
  }

  //Handle ActionEdit Button

  const handleEditButton = (item: ITodo) =>{
    setEditItem({ id: item.id, message: item.message, input: item.input });

  }

  //Edit Handler OnChange
  const handleEditOnChange = (event: any) => {
    const {name, value} = event.target;
    if(name === "editInput") {
      setEditItem({ ...editItem, input: value });
    }
    else if(name === "editMessage") {
      setEditItem({...editItem, message:value});
    }
  }

  //Delete Handler Button
  const handleActionDelete = (id: string) =>{
    const valPopDelete =  window.confirm("Are you sure you want to delete?");// prompt before deletion
    if(valPopDelete){
      const newInputList = input_list.filter((item) => item.id !== id);
      setInputList(newInputList);
    }
  }

  //Update Handler Button
  const handeUpdateButton = () =>{
    const valPopUpdate =  window.confirm("Are you sure you want to update this?");// prompt before update
    if(valPopUpdate){
      const newList = input_list.map(item => {
      if (item.id === editItem.id) {
        return editItem;
        }
        return item;
      });
      setInputList([...newList]);
      setEditItem({ id: '', input: '', message: '' });
    }
  }

 //Cancel Handler Button 
  const handleCancelButton = () =>{
  setEditItem ({id:'', input:'', message:''});
  }

  //Check Handler
  const checkBoxHandler = () => {
    setShowTable(prevState => !prevState);
  }

  return (
    <div>
      <Navbar />
      <Box
        sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '20vh',
        marginTop:'5%',
        marginLeft: '26%',
        width: '50%', /* Adjusted width */
        backgroundColor: 'rgba(255, 255, 255, 0.5)', // White color with 50% transparency
        backdropFilter: 'blur(10px)', // Optional: adds a blur effect to the background
        borderRadius: '10px', // Optional: adds rounded corners
        padding: '20px', // Optional: adds some padding
        boxShadow: '0 4px 8px rgba(0,0,0,0.1)', // Optional: adds a shadow
        }}>
        <Box sx={{ textAlign: 'center' }}>
          <ToDoForm
            inputHandler={inputHandler}
            messageHandler={messageHandler}
            btnAddHandler={btnAddHandler}
            temp_input={temp_input}
            temp_message={temp_message}
          />
          <div style={{ marginTop: '20px' }}>
            <FormControlLabel control={<Checkbox checked={showTable} onChange={checkBoxHandler} />} label="Show Table" />
            <p style={{ display: showTable ? 'none' : 'block' }}>Check "Show table" above to display list</p>
          </div>
          {showTable && (<TodoDisplay
            actionEdit={handleEditButton}
            editItem={editItem}
            actionDelete={handleActionDelete}
            inputlist={input_list}
            handleEditOnChange={handleEditOnChange}
            handeUpdateButton={handeUpdateButton}
            handleCancelButton={handleCancelButton}
          />)}
        </Box>
      </Box>
    </div>
  );
}

export default Home;
