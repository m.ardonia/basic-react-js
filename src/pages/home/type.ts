export interface IProp {}

export interface IState {
    temp_input: string;
    temp_message: string;
    input_list: ITodo[];
    showTable?: boolean;
    editItem: ITodo;
    

}

export interface ITodo {
    id: string;
    input: string;
    message: string;
}

export interface ItodoFormProp {
    inputHandler: (event: React.ChangeEvent<HTMLInputElement>) => void;
    messageHandler: (event: React.ChangeEvent<HTMLTextAreaElement>) => void;
    btnAddHandler: () => void;
    temp_input: string;
    temp_message: string;
   
}

export interface ITodoDisplayProp{
    inputlist: ITodo[];
    actionDelete: (value: any) => void;
    actionEdit: (value: any) => void;
    handleEditOnChange: (event: any) => void;
    handeUpdateButton: (event: any) => void;
    handleCancelButton:(event: any) => void;
    // closeDialog:() => void;
    // itemEdit:(event: any) => void;
    
    
    editItem?: ITodo

   
}