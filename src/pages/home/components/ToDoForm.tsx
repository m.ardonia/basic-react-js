import React from "react";
import "../style.css";
import { ItodoFormProp } from "../type";
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Stack from "@mui/material/Stack";
import { TextField, Box } from "@mui/material";
import TodoAddJTlogo from './TodoAddJTlogo.png'


const ToDoForm: React.FC<ItodoFormProp> = (props) => {
    const [open, setOpen] = React.useState(false);
    const { inputHandler, messageHandler, temp_input, temp_message, btnAddHandler } = props;

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handClose = () => {
        const shouldClose = window.confirm('Are you sure you want to cancel?');
        if (shouldClose) {
            setOpen(false);
        }
    };


    const AddItem = () => {

        if (!temp_input && !temp_message) {
            alert('Fields are empty. Please fill in the empty fields.');
          } else if (!temp_input) {
            alert('Title field is empty.');
          } else if (!temp_message) {
            alert('Message field is empty.');
          } else {
            btnAddHandler();
            handClose2(); 
        }

    };

    const handClose2 = () => {
            setOpen(false);
    };

    return (
        <>
            <Button variant="contained" color="success" onClick={handleClickOpen}>
                Add List
            </Button>
            <Dialog
                open={open}
                onClose={handClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                fullWidth
            >
                <Box
                    sx={{ 
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <DialogContent>
                        <DialogTitle id="alert-dialog-title">
                        <img src={TodoAddJTlogo} alt="Logo" style={{ width: '140px', height: '40px' }} />
                        </DialogTitle>
                        <Stack spacing={1} margin={1}>
                            <TextField name="message" onChange={inputHandler} value={temp_input} id="outlined-basic" variant="outlined" label='Title' fullWidth></TextField>
                            <TextField name="title" onChange={messageHandler} value={temp_message} id="outlined-textarea" multiline variant="outlined" label='Message' fullWidth></TextField>
                        </Stack>
                        <DialogActions>
                            <Button onClick={AddItem} variant="contained" color="success">ADD</Button>
                            <Button onClick={handClose} variant="contained" autoFocus color="warning">Cancel</Button>
                        </DialogActions>
                    </DialogContent>
                </Box>
            </Dialog>
            </>
    );
}

export default ToDoForm;
