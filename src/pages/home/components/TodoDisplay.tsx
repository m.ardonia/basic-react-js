import "../style.css";
import React from "react";
import { ITodoDisplayProp } from "../type";

//MUI Imports
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { TextField, Button, Box } from "@mui/material";
import Typography from '@mui/material/Typography';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import TodoUpdateJTlogo from './TodoUpdateJTlogo.png'




const TodoDisplay: React.FC<ITodoDisplayProp> = (props) => {

  const { inputlist,  editItem, actionDelete, actionEdit, handleEditOnChange, handeUpdateButton, handleCancelButton } = props;
  const [open, setOpen] = React.useState(false);

  const handClose2 = () => {
    setOpen(false);
  };

  const handOpen = () => {
    
    setOpen(true);
    
  };

  return (
    <>  
      <TableContainer component={Paper} sx={{ width: '450px', marginTop: '20px', padding:'10px' }}>
        <Table >
          <TableHead>
                  <TableRow>
                    <TableCell>Title</TableCell>
                    <TableCell>Message</TableCell>
                    <TableCell>Action</TableCell>
                  </TableRow>
          </TableHead>
            <TableBody >
                {inputlist.map((items, index) => {
                  if (items.id === editItem?.id && items.id) {
                    return (
                        <Dialog 
                          open={open}
                          onClose={handClose2}
                          aria-labelledby="alert-dialog-title"
                          aria-describedby="alert-dialog-description"
                          fullWidth
                        >
                           <Box>
                            <DialogContent >
                              <DialogTitle id="alert-dialog-title">
                                <img src={TodoUpdateJTlogo} alt="Logo" style={{ width: '160px', height: '40px' }} />
                              </DialogTitle>
                              <TableRow key={items.id + index}>
                                  <TableCell >
                                    <TextField label='Title' name="editInput" type="text" value={editItem.input} onChange={handleEditOnChange}/>
                                  </TableCell>
                                  <TableCell >
                                    <TextField label='Message'  name="editMessage" type="text" value={editItem.message} onChange={handleEditOnChange}/>
                                  </TableCell>
                                  <TableCell >
                                    <DialogActions>
                                      <Button  variant='contained' color="success" onClick={handeUpdateButton} >Update</Button>
                                      <Button  variant='contained' color="warning" onClick={handleCancelButton} >Cancel</Button>
                                    </DialogActions>
                                  </TableCell>
                                  
                            </TableRow>
                            </DialogContent>
                          </Box>
                          
                        </Dialog>
                        
                      );
                  } else {
                    return (  
                      <TableRow key={items.id + index} >
                        <TableCell>
                          {items.input}
                        </TableCell>
                        <TableCell>{items.message}</TableCell>
                        <TableCell sx={{ display: 'flex', gap: '5px' }} >
                          <Button variant='contained' color="success"    onClick={() => {handOpen();actionEdit(items);}}  > Edit </Button> 
                          <Button variant='contained' color="warning"  onClick={() => actionDelete(items.id)} > Delete </Button>
                        </TableCell>
                      </TableRow>
                    );
                  }   
                })}
            </TableBody>
        </Table>
        {inputlist.length === 0 && (
          <Typography variant="h5" component="h5" sx={{padding: '20px'}}>
            You have no list.
          </Typography>
        )}
      </TableContainer>
    </>
  );
}

export default TodoDisplay;