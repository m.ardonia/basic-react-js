export interface IProp{}

export interface IState {
    userPost: userPost[];
}

interface userPost{
    _id: number;
    title: string;
    message: string;
}