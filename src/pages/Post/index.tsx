import React, {useState, useEffect} from "react";
import axios from "axios";
import { IState } from "./type";
import { useNavigate } from "react-router-dom";
import Navbar from "../../components/navbar";
import './style.css'

const Post: React.FC = () =>{
   
    const [userPost, setPostList] = useState<IState["userPost"]>([]);
    const [title, setTitle] = useState('');
    const [message, setMessage] = useState('');
    const navigate = useNavigate();

    //display automatically the table
    useEffect (() => {
        handlePostRequestUsers();
    }, []);

    //function to display the table data from API
    const handlePostRequestUsers = async () =>{
        try {
            const response = await axios.get ("http://localhost:5173/api/post");
            setPostList(response.data);
           
        } catch (error) {
            alert('ERROR!');
        }
    };

    // Function for add button
    const handlePostAddButton = async () =>{
        try {
            // const newUser = {name: name, email: email, password:password, role: role} | another method , just add the variable after ,
            // console.log(newUser);
            //post the data
            const response = await axios.post("http://localhost:5173/api/post", {title:title, message:message});
            if(response.status == 200){ //reset
                handlePostRequestUsers();
                setTitle("");
                setMessage("");
            //    console.log({status});
            }
        } catch (error) {
            console.log(error); //will trigger if error encountered
        }
    }

    // Function for getting the value
    const handlePostInputChange = (e: any) => {
        const {name, value} = e.target;
        switch(name){
            case 'title':
                return setTitle(value);
            case 'message':
                return setMessage(value)
            default:
                return;
        }
    }

    // Delete function
    const handlePostDelete = async (id: number) => {
        try{
            await axios.delete(`http://localhost:5173/api/post/${id}`)
            setPostList(prevPostList => prevPostList.filter(item => item._id !== id))
        }
        catch (error) {
            console.log(error); //will trigger if error encountered
        }
    }

    // Function for log out
    const logOutHandler = () =>{
        localStorage.removeItem("Token")
        navigate("/");
    }

    return (
        <div>
            <Navbar/>
            <div>
                
                <h2>USER</h2>
                <span><a href="/"> Home </a>|</span>
                <span><a href="#"> About </a>|</span>
                <span><a href="/post"> Post </a>|</span>
                <span><a href="/" onClick={logOutHandler}> Log out </a></span> <br />

                <label>Name </label>
                <input name="title" type="text" onChange={handlePostInputChange} value={title} /><br/>
                <label>Message </label>
                <textarea name="message" onChange={handlePostInputChange} value={message}></textarea>
                <button onClick={handlePostAddButton}> ADD </button>

                <table border={1}>
                    <tbody>
                        <tr>
                        <th> Title </th>
                        <th> Message</th>
                        <th> Action</th>
                        </tr>
                        {userPost.map((items) => {
                        return (
                            <tr key={items._id}>
                                <td>{items.title}</td>
                                <td>{items.message}</td>
                                <td>
                                    <button>EDIT</button>
                                    <button onClick={()=>handlePostDelete(items._id)}>DELETE</button>
                                </td>
                            </tr>
                        )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Post;