export interface IProp{}

export interface IState {
    userList: userList[];
}

interface userList{
    _id: string;
    name: string;
    email: string;
    role: string;
}