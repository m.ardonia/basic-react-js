import React, {useState, useEffect} from "react";
import axios from "axios";
import { IState } from "./type";
// import { useNavigate } from "react-router-dom";
import Navbar from "../../components/navbar";
import './style.css';

const Users: React.FC = () =>{
   
    const [userList, setUserList] = useState<IState['userList']>([]);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [role, setRole] = useState('');
    // const navigate = useNavigate();

    //display automatically the table
    useEffect (() => {
        handleRequestUsers();
    }, []);

    //function to display the table data from API
    const handleRequestUsers = async () =>{
        try {
            const response = await axios.get ("http://localhost:5173/api/user");
            setUserList(response.data);
        } catch (error) {
            alert('ERROR!');
        }
    };

    // Function for add button
    const handleAddButton = async () =>{
        try {
            // const newUser = {name: name, email: email, password:password, role: role} | another method , just add the variable after ,
            // console.log(newUser);
            //post the data
            const response = await axios.post("http://localhost:5173/api/user", {name: name, email: email, password:password, role: role});
            console.log(response);
            if(response.status == 200){ //reset
                handleRequestUsers();
                setName("");
                setEmail("");
                setPassword("");
                setRole("");
               
            }
        } catch (error) {
            console.log(error); //will trigger if error encountered
        }
    }

    // Function for getting the value
    const handleInputChange = (e: any) => {
        const {name, value} = e.target;
        switch(name){
            case 'name':
                return setName(value);
            case 'email':
                return setEmail(value);
            case 'password':
                return setPassword(value);
            case 'role':
                return setRole(value);
                default:
                    return;
        }
    }

    // Function for log out
    // const logOutHandler = () =>{
    //     localStorage.removeItem("Token")
    //     navigate("/");
    // }

    return (

        <div>
            <Navbar/>
            <div>
                <h2>USER</h2>
                {/* <span><a href="/"> Home </a>|</span>
                <span><a href="#"> About </a>|</span>
                <span><a href="/post"> Post </a>|</span>
                <span><a href="/" onClick={logOutHandler}> Log out </a></span> <br /> */}

                <label>Name </label>
                <input name="name" type="text" onChange={handleInputChange} value={name} /><br/>
                <label>Email </label>
                <input name="email" type="text" onChange={handleInputChange} value={email} /><br/>
                <label>Password </label>
                <input name="password" type="password" onChange={handleInputChange} value={password} /><br/>
                <label>Role </label>
                <select name="role" onChange={handleInputChange} value={role}>
                    <option value="INTERN">INTERN</option>
                    <option value="TEAMLEAD">TEAMLEAD</option>
                    <option value="ADMIN">ADMIN</option>
                </select>
                <button onClick={handleAddButton}> ADD </button>

                <table border={1}>
                    <tbody>
                        <tr>
                        <th> Name </th>
                        <th> Email</th>
                        <th> Password</th>
                        </tr>
                        {userList.map((items) => {
                        return (
                            <tr key={items._id}>
                                <td>{items.name}</td>
                                <td>{items.email}</td>
                                <td>{items.role}</td>
                            </tr>
                        )
                        })}
                    </tbody>
                </table>
            </div>
        </div>

    )
}

export default Users;