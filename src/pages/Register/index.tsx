import React, { useState } from "react";
import { Button, Dialog, DialogTitle, DialogContent, DialogActions, Stack, TextField, IconButton, Typography } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import JTLogo from './JTlogo.png';

const Register: React.FC<{ closeDialog: () => void }> = ({ closeDialog }) => {
    const [showPassword, setShowPassword] = useState(false);
    const [password, setPassword] = useState ("");
    const [username, setUsername] = useState ("");

    const inputUserHandler = (event: React.ChangeEvent<HTMLInputElement>) =>{
        const {value} = event.target
        setUsername(value);
    }

    const inputPassHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {value} = event.target
        setPassword(value);
    }

    //Register button function here.....

    const handleCheckboxChange = () => {
      setShowPassword(!showPassword);
    };

  
    return (
        <div className="register-main-container">
            <Dialog open={true} onClose={closeDialog} fullWidth aria-labelledby="dialog-title" aria-describedby="dialog-content">
            <DialogTitle id="dialog-title"> 
                <Typography variant="h6"/><img src={JTLogo} alt="Logo" style={{ width: '130px', height: '45px' }} /> 
                <IconButton style={{ float: 'right' }} onClick={closeDialog}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent>
                <Stack spacing={2} margin={2}>
                    <TextField name="username" value={username} onChange={inputUserHandler} variant="outlined" label='Username'></TextField>
                    <TextField type={showPassword ? 'text' : 'password'} variant="outlined" label='Password'></TextField>
                    <TextField type={showPassword ? 'text' : 'password'} value={password} onChange={inputPassHandler} variant="outlined" label='Confirm Password'></TextField>
                    <FormControlLabel control={<Checkbox checked ={showPassword} onChange={handleCheckboxChange}/>} label="Show Password" />
                </Stack>
            </DialogContent>
            <DialogActions>
                <Button variant="contained" color="primary">Confirm Registration</Button>
                <Button variant="contained" color="error" onClick={closeDialog}>Cancel</Button>
            </DialogActions>
         </Dialog>
        </div>
    );
};

export default Register;