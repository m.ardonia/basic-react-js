export interface IProp{};

export interface RegisterFormProp {
    setUserName: (event: any) => void;
    setPassWord: (event: any) => void;
    btnRegister: () => void;
    closeDialog: (event: any) => void;
    username: string;
    password: string;
}