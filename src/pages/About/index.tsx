import React, { useState } from 'react';
import { IProp } from './type';
import JTsvg from './JTsvg.png';

//MUI imports
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
// import FormControlLabel from '@mui/material/FormControlLabel';
// import Checkbox from '@mui/material/Checkbox';
import { FormControlLabel, IconButton, Typography } from '@mui/material';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';

const LoginPage: React.FC = (props: IProp) => {

    const [showPassword, setShowPassword] = useState(false);

    const formStyle = {
        background: 'rgba(255, 255, 255, 0.8)', // Transparent ni
        padding: '30px',
        borderRadius: '10px',
        width: '25%', // Adjust the width as needed
        margin: '0 auto', // Center the box horizontally
        
    };

    const handleShowPass = () => {
        setShowPassword(!showPassword);
    };
    return (
        <>
            {/* Box center display */}
            <Box
                sx={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '100vh',
                
            }}>
                {/* Box style */}
                <Box sx={formStyle}>
                    {/* Login Form */}
                    <form >
                        <Typography variant="h6"/><img src={JTsvg} alt="Logo" style={{ width: '130px', height: '40px' }}/>
                        {/* User field */}
                        <TextField
                            label="Username"
                            variant="outlined"
                            fullWidth
                            margin="normal"
                        />

                        {/* Password field */}
                        <TextField
                            label="Password"
                            variant="outlined"
                            fullWidth
                            margin="normal"
                            type= {showPassword ? 'text' : 'password'}

                            //For eye icon inside password field
                            InputProps={{
                                endAdornment:(
                                    <IconButton 
                                        onClick={handleShowPass} 
                                        edge='end' 
                                        sx={{paddingRight:'30px'}}
                                    >
                                        {showPassword ? <VisibilityIcon/> : <VisibilityOffIcon/>}
                                    </IconButton>
                                ),
                            }}
                        />

                        {/* Remember me and Forgot Password */}
                        <Box sx={{ display: 'flex', alignItems: 'center' }}>
                            
                            {/* For Checkbox */}
                            <FormControlLabel 
                                control={<Checkbox defaultChecked/> } 
                                label='Remember me'
                                sx={{padding:'6px'}}
                            />

                             {/* For reset password */}
                            <Link 
                                href="#" 
                                underline="hover" 
                                color="primary"
                                sx={{marginLeft:'35%'}}
                                fontFamily={'Arial, sans-serif'}
                            >
                                Forgot Password?
                            </Link>
                        </Box>

                        {/* Login Button */}
                        <Button 
                            variant="contained" 
                            color="primary" 
                            type="submit" 
                            fullWidth 
                        > 
                            Login 
                        </Button>
                    </form>
                </Box>
            </Box>
        </>
    );
};

export default LoginPage;