export interface IProp{};

export interface LoginFormProp {
    userName?: (event: any) => void;
    passWord?: (event: any) => void;
    btnLogin?: () => void;
    username?: string;
    password?: string;
    showPass?: boolean;
    closeDialog: () => void;
    openRegisterForm: () => void;
}