import React, { useState } from "react";
import { Button, Dialog, DialogTitle, DialogContent, DialogActions, Stack, TextField, IconButton, Typography } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import JTLogo from './JTlogo.png';
import { useNavigate } from "react-router-dom";
import { accounts } from "../../utils/authenAccounts";
import { LoginFormProp } from "./type";

const Login: React.FC<LoginFormProp> = (props)  => {
    const { closeDialog, openRegisterForm } = props;
    const [showPassword, setShowPassword] = useState(false);
    const [password, setPassword] = useState ("");
    const [username, setUsername] = useState ("");
    const navigate = useNavigate();

    //Username Handler
    const inputUserHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {value} = event.target
        setUsername(value);
    }

    //Password Handler
    const inputPassHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {value} = event.target
        setPassword(value);
    }

    //Login Btn Handler
    const btnLogin = () => {
        const token = {username, password}
 
        const authenticated = accounts.map(item => JSON.stringify(item)).includes(JSON.stringify(token));
        if (!username && !password){
            alert('Fields are empty. Please fill in the empty fields.');
        }
        else if (!username) {
            alert('Username field is empty.');
        } 
        else if (!password) {
            alert('Password field is empty.');
        }
        else if(authenticated){
            localStorage.setItem('Token', JSON.stringify(token));
            navigate('/');
        }
        else{
         alert ("Invalid Account");
        }
        
     };

    const handleCheckboxChange = () => {
      setShowPassword(!showPassword);
    };

    return (
        <Dialog open={true} onClose={closeDialog} fullWidth aria-labelledby="dialog-title" aria-describedby="dialog-content">
            <DialogTitle id="dialog-title"> 
                <Typography variant="h6"/><img src={JTLogo} alt="Logo" style={{ width: '130px', height: '45px' }} /> 
                <IconButton style={{ float: 'right' }} onClick={closeDialog}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent>
                <Stack spacing={2} margin={2}>
                    <TextField onChange={inputUserHandler} value={username} variant="outlined" label='Username'></TextField>
                    <TextField value={password} onChange={inputPassHandler} type={showPassword ? 'text' : 'password'} variant="outlined" label='Password'></TextField>
                    <FormControlLabel control={<Checkbox checked ={showPassword} onChange={handleCheckboxChange}/>} label="Show Password" />
                </Stack>
            </DialogContent>
            <DialogActions>
                <Button onClick={btnLogin} variant="contained" color="success">Login</Button>
                <Button variant="contained" color="primary" onClick={openRegisterForm}>Register</Button>
            </DialogActions>
        </Dialog>
    );
};

export default Login;